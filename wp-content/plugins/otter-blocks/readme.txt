=== Otter Blocks - Awesome Gutenberg Blocks ===
Contributors: themeisle, hardeepasrani
Tags: gutenberg, block, services block, pricing block, testimonials block, post grid block, google map block, font awesome block, sharing icons block, about author block, accodion block, pie chart block, notice block, tweet block
Requires at least: 4.9    
Tested up to: 5.0  
Requires PHP: 5.4  
Stable tag: trunk
License: GPLv3  
License URI: https://www.gnu.org/licenses/gpl-3.0.en.html  

Create beautiful and attracting posts, pages, and landing pages with Otter Blocks. Otter Blocks comes with dozens of Gutenberg blocks that are all you need to build beautiful pages.

== Description ==

Create beautiful and attracting posts, pages, and landing pages with Otter Blocks. Otter Blocks comes with dozens of Gutenberg blocks that are all you need to build beautiful pages.

See what you can do with Otter Blocks: [https://demo.themeisle.com/otter-blocks/](https://demo.themeisle.com/otter-blocks/)

It has:

- Our Services Block
- Pricing Block
- Testimonials Block
- Post Grid Block
- Plugin Card Block
- Google Map Block
- Font Awesome Icon Block
- Sharing Icons Block
- About Author Block
- Accordion Block
- Pie Chart Block
- Notice Block
- Click to Tweet Block

== Installation ==
Activating this plugin is just like any other plugin. If you’ve uploaded the plugin package to your server already, skip to step 5 below:

1. Install using the WordPress built-in Plugin installer, or Extract the zip file and drop the contents in the wp-content/plugins/ directory of your WordPress installation.
2. Activate the plugin through the ‘Plugins’ menu in WordPress.
3. Go to Gutenberg editor and play around with the block.

== Screenshots ==

1. Otter Blocks
2. Pricing Block
3. Service Block
4. Pie Chart Block
5. Plugin Card Block