<?php
/**
 * Free Downloads - File Functions
 * 
 * Managing files and filepath functions
 * 
 * @version 3.0.9
 * @author  Square One Media
 */

if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'somdn_on_activate', 'somdn_on_activate_file_cron', 20 );
function somdn_on_activate_file_cron() {
	if ( ! wp_next_scheduled ( 'somdn_delete_download_files_event' ) ) {
		wp_schedule_event( time(), 'hourly', 'somdn_delete_download_files_event' );
	}
	$upload_dir = wp_upload_dir();
	$zip_path = $upload_dir['basedir'] . '/download-now-uploads';
	if ( ! file_exists( $zip_path ) ) {
		mkdir( $zip_path, 0777, true );
	}
}

add_action( 'somdn_on_deactivate', 'somdn_on_deactivate_file_cron', 20 );
function somdn_on_deactivate_file_cron() {
	wp_clear_scheduled_hook( 'somdn_delete_download_files_event' );
	$upload_dir = wp_upload_dir();
	array_map( 'unlink', glob( $upload_dir['basedir'] . '/download-now-uploads/*' ) );
	$zip_path = $upload_dir['basedir'] . '/download-now-uploads/';
	rmdir( $zip_path );
}

add_action( 'somdn_delete_download_files_event', 'somdn_delete_download_files' );
function somdn_delete_download_files() {
	$upload_dir = wp_upload_dir();
	array_map( 'unlink', glob( $upload_dir['basedir'] . '/download-now-uploads/*' ) );
}

/**
 * Return an array of product files from download or post data
 *
 * @param $download_data The download file array
 * @return array Just the files selected as numbers
 */
function somdn_get_selected_product_files( $download_data ) {

	$selected_files = array();

	// No $download_data passed through
	if ( empty( $download_data ) )
		return $selected_files;

	// No action in $download_data
	if ( empty( $download_data['action'] ) )
		return false;

	// No product id in $download_data
	if ( empty( $download_data['somdn_product'] ) )
		return false;

	$product = somdn_get_product( $download_data['somdn_product'] );

	// Return for good measure if product not found
	if ( empty( $product ) )
		return $selected_files;

	return apply_filters( 'somdn_get_selected_product_files', $selected_files, $download_data );

}

add_filter( 'somdn_get_selected_product_files', 'somdn_get_selected_product_files_standard', 15, 2 );
function somdn_get_selected_product_files_standard( $selected_files, $download_data ) {

	$action = sanitize_key( $_POST['action'] );
	$actions = somdn_get_download_actions();

	if ( in_array( $action, $actions ) ) {

		// This is a standard product download submitted by a frontend download form
		$product = somdn_get_product( $download_data['somdn_product'] );
		$downloads = somdn_get_files( $product );
		$downloads_count = count( $downloads );

		$checked_count = 0;
		
		while ( $checked_count < $downloads_count ) {
		
			$checkbox_number = $checked_count + 1;
			$checkbox_id = 'somdn-download-file-' . strval( $checkbox_number );

			if ( isset( $_POST[$checkbox_id] ) && $_POST[$checkbox_id] ) {
				array_push( $selected_files, ( $checked_count ) );
			}
			
			$checked_count++;
			
		}

	}

	return $selected_files;

}

/**
 * Return an array of all file paths to download
 *
 * @param $product The WooCommerce product object
 * @param $downloads The download file array
 * @return array Just the file paths
 */
function somdn_get_file_paths( $product, $downloads ) {
	$download_files = array();
	foreach ( $downloads as $key => $each_download )  {
		$file_path = somdn_get_download_filepath( $product, $key, $each_download, $each_download['id'] );
		array_push( $download_files, $file_path );
	}
	return $download_files;
}

/**
 * Return the filtered file url/path to download
 *
 * @param  object $product        The product object
 * @param  int    $key            The key in the array of products
 * @param  array  $download_array The array of file info ( id, name, file )
 * @param  int    $download_id    The ID of the download in the downloads array
 * @return string                 Filepath/URL for the file (filtered)
 */
function somdn_get_download_filepath( $product, $key, $download_array, $download_id ) {
	$file_path = $download_array['file'];
	return apply_filters( 'somdn_download_path', $file_path, $product, $key, $download_array, $download_id );
}

function somdn_get_download_filepath_raw( $product, $key, $download_array, $download_id ) {
	$file_path = $download_array['file'];
	return $file_path;
}

/*
 * Check whether the file we're downloading is stored locally or on an external server.
 * First check if the file is duplicated into the temporary folder, if not then check by using the url headers
 */
function somdn_is_local_file( $url ) {

	$url_array = explode( '/', $url );
	$filename = end( $url_array );

	$upload_dir = wp_upload_dir();
	$local_path = $upload_dir['basedir'] . '/download-now-uploads/' . $filename;
	$url_path = $upload_dir['baseurl'] . '/download-now-uploads/' . $filename;

	if ( file_exists( $local_path ) ) {
		//echo '<p>Exists</p>';
		return true;
	} else {
		//echo '<p>Not Local</p>';
		$headers = get_headers( $url );
		return stripos( $headers[0], '200 OK' ) ? false : true ;
	}

}

/*
 * Determines whether the file is locally stored or not.
 * If local then open unsing Google Dosc Viewer, otherwise download as normal.
 */
function somdn_show_pdf( $file, $product_id ) {

	//somdn_write_log( 'Download Now - show pdf' );

	if ( somdn_is_local_file( $file ) ) {

		// File is stored locally. Now to check if the file has already been duplicated (security) for viewing into the temporary folder.
		// If it has then $duplicate_file variable is set to $url_path, otherwise run the normal duplicate procedure.

		$url_array = explode( '/', $file );
		$filename = end( $url_array );

		$upload_dir = wp_upload_dir();
		$local_path = $upload_dir['basedir'] . '/download-now-uploads/' . $filename;
		$url_path = $upload_dir['baseurl'] . '/download-now-uploads/' . $filename;

		if ( ! file_exists( $local_path ) ) {

			// File not already duplicated

			$duplicate_file = somdn_duplicate_pdf( $file );
			if ( ! $duplicate_file ) {
				somdn_do_download( $file, $product_id );
				return;
			}

		} else {

			// File has already been duplicated
			$duplicate_file = $url_path;

		}

	} else {
		// File is hosted externally, download as normal
		somdn_do_download( $file, $product_id );
		return;
	}

	$google_path = 'https://docs.google.com/viewerng/viewer?url=' . $duplicate_file;
	//echo '<script>window.open("' . $google_path . '")</script>';

	do_action( 'somdn_count_download', $product_id );
	
	wp_redirect( $google_path );
	//wp_redirect( get_the_permalink( $product_id ) . '?somdn_pdf=' . $google_path );
	//somdn_write_log( 'Download Now - redirect' );
	exit();

}

/**
 * Duplicate a local PDF file for viewing externally
 *
 * @param  string $file_path  The file path of the original PDF
 * @return string $newfileurl Filepath/URL for the file
 */
function somdn_duplicate_pdf( $file_path ) {

	$upload_dir = wp_upload_dir();
	$dn_folder  = $upload_dir['basedir'] . '/download-now-uploads';

	if ( ! file_exists( $dn_folder ) ) {
		mkdir( $dn_folder, 0777, true );
	}

	$path         = parse_url( $file_path, PHP_URL_PATH );
	$abs_filepath = $_SERVER['DOCUMENT_ROOT'] . $path;
	$uri          = ltrim( $path, '/' );

	$now        = DateTime::createFromFormat( 'U.u', microtime( true ) );
	$code1      = $now->format( "ms" );
	$code2      = $now->format( "u" );
	$downloadID = get_current_user_id() . $code1 . $code2;

	$newfileurl = $upload_dir['baseurl'] . '/download-now-uploads/' . $downloadID . '.pdf';
	$newfile    = $upload_dir['basedir'] . '/download-now-uploads/' . $downloadID . '.pdf';

	if ( ! copy( $abs_filepath, $newfile ) ) {
		return false;
	} else {
		return $newfileurl;
	}

}

/**
 * Create a ZIP archive from passed in filepaths
 *
 * @param array  $files The file paths
 * @param string $destination The location to save the ZIP archive
 * @param bool   $overwrite whether to overwrite an existing archive if found
 * @param bool   $distill_subdirectories remove subdirectories
 * @return string Filepath/URL for the file
 */
function somdn_create_zip( $files = array(), $destination = '', $overwrite = false, $distill_subdirectories = true ) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
		    if ($distill_subdirectories) {
		        $zip->addFile($file, basename($file) );
		    } else {
		        $zip->addFile($file, $file);
		    }
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		//print_r($zip);
		
		//$newfilepath = $zip['filename'];
		$newfilepath = $zip->filename;
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		if ( file_exists($destination) ) {
			return $newfilepath;
		} else {
			return false;
		}
	}
	else
	{
		return false;
	}
}

/**
 * Get filesize function. In testing.
 * 
 */
//foreach( $downloads as $key => $each_download )  {
//		$path = parse_url( $each_download['file'], PHP_URL_PATH );
//		$abs_filepath = $_SERVER['DOCUMENT_ROOT'] . $path;
//		$size = filesize($abs_filepath);
//		echo formatBytes($size, 1);
//}
//function formatBytes($size, $precision = 2) {
//	$base = log($size, 1024);
//	$suffixes = array( '', ' KB', ' MB', ' G', ' T' );   
//	return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
//}